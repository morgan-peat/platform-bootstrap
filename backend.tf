# Enables terraform cloud CLI-driven workflow.
# This enables `terraform plan` from the command line. Apply is not
# permitted as this is a VCS-linked terraform workspace.
# See https://developer.hashicorp.com/terraform/language/settings/terraform-cloud

terraform {
  cloud {
    organization = "mp-demo-org"

    workspaces {
      name = "platform-bootstrap"
    }
  }
}