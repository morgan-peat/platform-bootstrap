
# Configures the Terraform Cloud organization, creating
# workspaces, teams etc.
# AuthN is via the TFE_TOKEN environment variable set in this
# repository's TFC workspace.
# The TFE_ORGANIZATION env variable sets the org.
provider "tfe" {}

# Configures Gitlab projects.
# AuthN is via the GITLAB_TOKEN environment variable set in this
# repository's TFC workspace.
provider "gitlab" {}

# Enables access to secrets stored in vault.
# Vault authN is by workload identity using env variables
# stored in this workspace.
# See https://developer.hashicorp.com/terraform/cloud-docs/workspaces/dynamic-provider-credentials/vault-configuration.
provider "vault" {}

# HashiCorp internal provider that issues temporary credentials
# which the AWS provider uses
provider "doormat" {}

data "doormat_aws_credentials" "creds" {
  role_arn = var.aws_role_arn
}

provider "aws" {
  region     = "eu-west-2" # London
  access_key = data.doormat_aws_credentials.creds.access_key
  secret_key = data.doormat_aws_credentials.creds.secret_key
  token      = data.doormat_aws_credentials.creds.token
}
