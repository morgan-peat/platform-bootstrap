# Creates an environment for an application called "Lighthouse"

module "lighthouse_app" {
  source = "./modules/app"

  name                = "Lighthouse"
  name_slug           = "lighthouse"
  description         = "Lighthouse application to show how an application can be created through IaC."
  gitlab_namespace_id = data.gitlab_current_user.default.namespace_id
  developers = [
    "morgan_peat_test",
    "morgan_peat_hashi",
  ]
}

module "lighthouse_app_dev" {
  source = "./modules/app-env"

  app_name_slug = module.lighthouse_app.name_slug
  environment   = "dev"
  description   = "DEV workspace for Lighthouse"

  developer_write_access = true

  tfe_project_id         = module.lighthouse_app.tfc_project_id
  tfc_developers_team_id = module.lighthouse_app.tfc_developers_team_id
  terraform_version      = "~> 1.4.0"
  gitlab_project_id      = module.lighthouse_app.gitlab_project_id
  vault_policies         = ["default", vault_policy.platform_app_env_template.name]
}

module "lighthouse_app_prd" {
  source = "./modules/app-env"

  app_name_slug = module.lighthouse_app.name_slug
  environment   = "prd"
  description   = "PRD workspace for Lighthouse"

  tfe_project_id         = module.lighthouse_app.tfc_project_id
  tfc_developers_team_id = module.lighthouse_app.tfc_developers_team_id
  terraform_version      = "~> 1.4.0"
  gitlab_project_id      = module.lighthouse_app.gitlab_project_id
  vault_policies         = ["default", vault_policy.platform_app_env_template.name]
}

module "lighthouse_app_dev2" {
  source = "./modules/app-env"

  app_name_slug = module.lighthouse_app.name_slug
  environment   = "dev2"
  description   = "DEV2 workspace for Lighthouse"

  developer_write_access = true

  tfe_project_id         = module.lighthouse_app.tfc_project_id
  tfc_developers_team_id = module.lighthouse_app.tfc_developers_team_id
  terraform_version      = "~> 1.4.0"
  gitlab_project_id      = module.lighthouse_app.gitlab_project_id
  vault_policies         = ["default", vault_policy.platform_app_env_template.name]
}
