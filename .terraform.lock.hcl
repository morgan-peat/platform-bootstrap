# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "doormat.hashicorp.services/hashicorp-security/doormat" {
  version     = "0.0.5"
  constraints = "~> 0.0"
  hashes = [
    "h1:jx3fKLP+HC+O4RRXY5RxI1kM7WADtu62MroZGshAQu0=",
    "zh:0c65e91845974bbc12f8f4a5bffe859f7da76b6f0b101909980d444148ad1433",
    "zh:0cb5114ece10ecbd4139f90bff4854f3f3fa6ece2851fd744f804245754ca240",
    "zh:1c7ce0252053fca15b3909d89a805b5eb13a098c51cca5d0f86b9fcd77375d2a",
    "zh:39c9b76e3e31dfb91df2adbc852e1da9055cfa108a5db20fc3644ad354291e74",
    "zh:40b660de817530d2c7d6e52f8fefc7e4fe09226466a2fda357828dc8085acf78",
    "zh:41b1dfa5ce3404a9090a0a31b1fdbae4ea32ace1148ee3efe9bbbb3ce439895c",
    "zh:42e17bb14184a40f152ad980169e0b5ca44a247ee8eff9511e31f91b9603d85e",
    "zh:4e7a61cd264c978907a331f058a645c19131b211814e96ccda241570bec9cf11",
    "zh:59c542ad43c7377a05dd37496a8fcd5d133f383f66169ed5408c64d3bdfc6f6f",
    "zh:6a10565b130df8b037f0d7c8fb5f44944295a4b54d6cc4e15e627fc5fe3b9994",
    "zh:8d466f973d1d3cee2f5c821e1604b471ca36c1bc886ba7a0eb9d333bf76e9c4b",
    "zh:9ac6bcc2fa0d63df0f571981bcb566a69e470f337d90355dfdd3f012ed113c37",
    "zh:b9f2ed2e2516e8fa523181b359abb00a64461a6e6495d1df559180b9013b5785",
    "zh:baa610c11c9bfdd28742383d2f06e01122b9795ca1b6371b1cfffc7299723ccb",
    "zh:c33b19cf24cf49f9728c527886906397188459ca5da1eb8e8ef00ec0e901d117",
    "zh:c379f34e71f13a750b265e0811a44501110fd8e153d676b25ea8286053d9637b",
    "zh:d736eda4daf72e011c1a64128359e0c0075c26146315d43e4a6ecd785887b0b3",
    "zh:dd119cb699ca5ea27d26b714d8df2c41b23d190d92425322ecfd18245592b47e",
    "zh:e614a56a73bf83810769bd450fffc695c0b3c5ba3827d712a8bdd8d20205ac6d",
    "zh:f8e92874ac9a53f880c776d9c2f45aa6249ec962d6410411b908270f2a2a4320",
    "zh:f99e02aeed91662bccb4bbdfd02725ef27d7a37255eee0c6406e00a7507e816e",
    "zh:fd2e3a38a96c039aa4cfcbbeda9cbf65160a8c44d65bf32539736e8f7f12dc7a",
  ]
}

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "16.0.3"
  constraints = "~> 16.0.0"
  hashes = [
    "h1:35qAhOTdNKCwmRKrwgTuPUfSdOPZh8nc4RWHDJ/dxI0=",
    "h1:4I7hc2txv3KR9byHUhl74sX+A4IbkICcRf0gLw9Cmu8=",
    "h1:5AowWDt4qlsfVv92IlVptSo2MgJH/mK3zlNzikhbtbI=",
    "h1:FYcyZfKSX4S4A4BFs5BeFhTRzSy81f/oeEf+puHV46M=",
    "h1:JZZ5YlXy94PksHB4qRzeIFwCyUy5VdG+9GNVZK3nLu0=",
    "h1:NBCRbq9yjFnIifKqWzcfyYsUNH454Mgn4IrIIK7VJbA=",
    "h1:RDuwB4If/4RvmNBa77+FvC+E0kfft7tih1QNSbw2QXI=",
    "h1:Vib9pOm+75QFe08n5jr79PeLoqOa5hkWNTXToEg50K4=",
    "h1:VuzAQgv5Da+OiaiA+DVbJ+mBT+GOueyNLqi6olZ3hwg=",
    "h1:fRJYRix+Bs68j/HP/RxFOyb1KssBA7+rIgZixvx9FiI=",
    "h1:h6b1HcBTxsP8wPeYEHfcI29T8bAAzRPDp1KjFFqri8A=",
    "h1:hzoPAu6x1A8rjBhmgZMNzhdUq0Jhpde8EIxTzirPYVg=",
    "h1:ilq8eG3NpIleX0q3GyXUJJiXLqyQYhP5AUIc5vIAWLo=",
    "h1:nQB7f2SoQQkZW6Lc7oaZ85LQYStvHS3Bd+vCnlTLdAo=",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "4.67.0"
  constraints = "~> 4.67.0"
  hashes = [
    "h1:5Zfo3GfRSWBaXs4TGQNOflr1XaYj6pRnVJLX5VAjFX4=",
    "zh:0843017ecc24385f2b45f2c5fce79dc25b258e50d516877b3affee3bef34f060",
    "zh:19876066cfa60de91834ec569a6448dab8c2518b8a71b5ca870b2444febddac6",
    "zh:24995686b2ad88c1ffaa242e36eee791fc6070e6144f418048c4ce24d0ba5183",
    "zh:4a002990b9f4d6d225d82cb2fb8805789ffef791999ee5d9cb1fef579aeff8f1",
    "zh:559a2b5ace06b878c6de3ecf19b94fbae3512562f7a51e930674b16c2f606e29",
    "zh:6a07da13b86b9753b95d4d8218f6dae874cf34699bca1470d6effbb4dee7f4b7",
    "zh:768b3bfd126c3b77dc975c7c0e5db3207e4f9997cf41aa3385c63206242ba043",
    "zh:7be5177e698d4b547083cc738b977742d70ed68487ce6f49ecd0c94dbf9d1362",
    "zh:8b562a818915fb0d85959257095251a05c76f3467caa3ba95c583ba5fe043f9b",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:9c385d03a958b54e2afd5279cd8c7cbdd2d6ca5c7d6a333e61092331f38af7cf",
    "zh:b3ca45f2821a89af417787df8289cb4314b273d29555ad3b2a5ab98bb4816b3b",
    "zh:da3c317f1db2469615ab40aa6baba63b5643bae7110ff855277a1fb9d8eb4f2c",
    "zh:dc6430622a8dc5cdab359a8704aec81d3825ea1d305bbb3bbd032b1c6adfae0c",
    "zh:fac0d2ddeadf9ec53da87922f666e1e73a603a611c57bcbc4b86ac2821619b1d",
  ]
}

provider "registry.terraform.io/hashicorp/tfe" {
  version     = "0.44.1"
  constraints = "~> 0.44.0"
  hashes = [
    "h1:0jEZgtgr+N25EWWTVkycIzP+FT5rBwgxyYlE6bSn4L0=",
    "zh:00efe5957430cb6d855a2f107e4b93ea72f74e4f3044bbacfc8cf4407f48cc59",
    "zh:01fd66c8dada0c3e5ac01cd738a9b1e77557e5dbfff0fb9137c4981619102996",
    "zh:097d0e1ed04faeaab2aca0319269f641fe114146388c75b7108e7976a8384d31",
    "zh:12d8720ea35933e74ffdbe0bf65888ce6de23c4c31fb1e1c84404a8afe5a55ef",
    "zh:5a49b8c4f93177e40a3399071cc49f42a11ce0c3b2b52aebc72ca3e29f47f91f",
    "zh:656fd901d831fecc86769ab175b7d0a5c2a172618254998c2ca8488f4d6c63d0",
    "zh:8f3bc0a28838e35c0567d7de044f7d94cf851814703acfd30696854bb5e37db4",
    "zh:911c02dd88721317af6494029387384002d52f62539d51fd6ac30b667c78dfc5",
    "zh:c266895f7ea65282e6ec7bdd473bb512345971891244efa3635edbf91f825867",
    "zh:d95ec293fa70e946b6cd657912b33155f8be3413e6128ed2bfa5a493f788e439",
    "zh:e71c815c8d09e3c7e239707a5ca756894ebd8ec4639f6910756ad85b83129443",
    "zh:fcd60145a34394863d4e049a1decc07063f827e1287e39406adbca6658cec476",
  ]
}

provider "registry.terraform.io/hashicorp/vault" {
  version     = "3.15.2"
  constraints = "~> 3.15.0"
  hashes = [
    "h1:Nev6nEf+2vRGQ+gw0Lvo45IxMNJjILMR67D1khIXCY8=",
    "zh:106ef32d71e6a9c4b71a097dff34b7b497a8b5a495223480b004eed5e690bf75",
    "zh:36409e772ebec1bd25f72fc792d771f3da07080e276505aed8d258ee410a4a52",
    "zh:369616473917aebaa9503afe54ba50e9b60f3648242bebcad45fcfec97d83a2d",
    "zh:6d4bd44204a25700cc18ae571ea4260f1d90daf654fbb47b3c1ce57a4ee5ad84",
    "zh:7373e4cbed3ac8623eff1f60842a4d7110cd0a4f4e0770ba09e7fb82b5a1feaf",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:7d1ef1115e66fad2269362a585ab4a70d89df1451e256b662878dd4a40560ff6",
    "zh:953183c58d5b7518f20ad2ff1e72848b59d31c326bf6beb06fb664214bf6fd7f",
    "zh:9a82527862557064a44f7dbd79bb32843a12f079eddfdaeab47eb5dd4f83a1b7",
    "zh:a0d659402f2b25547b06eb20fcc6a64828c7a4ad046b7d7e1e6a1facae2babe5",
    "zh:b403172fa7c20309eb2646de1976c17b5f7b9b32146b5f568cecfab439a56699",
    "zh:c20219be762ea5307f1503f3568f21b670e2c2cdb2fb219698b12d1b6c2523be",
  ]
}

provider "registry.terraform.io/morganpeat/environment" {
  version     = "0.1.4"
  constraints = "~> 0.1"
  hashes = [
    "h1:eIi/ZD8itIDPbQiX0zmtRRrt5thHuGdbHFIEZIfnAIo=",
    "zh:3465fce61ce4036754fe6bd6a999a788e8414566b94c1c4180e23594d36f6eaf",
    "zh:382ca28d093a05fd1f5d0e1e94b09a061621b9a808b5853ccdfd970fb6d54db1",
    "zh:3c489c85cbe687f4f56461365fdd2db9ef7c5214324235fef4afcd9acde3eeb5",
    "zh:4f6f6f50088050826e0dc14f84f672c28e54955ba94bfe4587febaff63b4fc73",
    "zh:853ec603084a357b59d70886c8be9e445efba7d126eab264265fc9dc3976a7b9",
    "zh:9675b7cf020e90aed7454e6c6467e5eb58197099a2edf188a8189b19ad91f4ed",
    "zh:a0ff34a04885bd21282652f21e751647ee46fb7e20f5c8bb5a323e468d16485a",
    "zh:a9a0c06c33ff25c875d2d68c6bb72afc6a95805cb5d5cd81a56528d8619a4cf4",
    "zh:c74f7cc581c75c5a28bbfbd7d1d585f854df2d4d0e52e72a7c0d1c642dd5fffe",
    "zh:df8170827d969921638aa8a4379a8f43e13ce1ab28941ba90894e82a2171484d",
    "zh:e908c3f909b9e53ca5428c3b099373a7e8913fbf14f86cadbb84646dfec2a8da",
  ]
}
