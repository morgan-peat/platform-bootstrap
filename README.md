# platform-bootstrap

Simple PoC that demonstrates an "application factory".

## Application onboarding

Each application to be onboarded to the platform executes the [app](./modules/app/) module. It onboards the application by creating

* A GitLab repository
* A TFC Project
* A TFC "developers" team

## Environment onboarding

Each environment for the application (e.g. development, production) uses the same terraform code in the application's repository but in its own TFC workspace.
For each environment, the [app-env](./modules/app-env/) module is executed. It creates

* A new TFC workspace for the environment
* Team access for the "developers" team
* VCS connection from the git repo to the workspace, so changes in the repo will cause terraform to execute

## CI/CD and GitOps

This PoC attempts to follow the GitOps model of development by keeping `main` branch as the "source of truth", though the model is adjusted slightly to achieve developer velocity.

### Where GitOps is used

Any "higher" environment (UAT, PRD) should always use GitOps and use `HEAD` of `main` branch as the Source Of Truth.  
This means that all changes are performed by executing a CI/CD pipeline against `main` branch. No humans may write or change anything directly (i.e. no "ClickOps").
`main` is a protected branch, meaning any changes to the code (therefore any changes to the actualy deployment) must be made by raising a Merge Request and gaining peer approval.

### Where GitOps is NOT used

Any "lower" environment (i.e. DEV) should allow developers to make changes directly, outside the constraints of a CI/CD pipeline. The chief reason for this is developer velocity: committing code to a branch, raising a MR, waiting for the CI/CD pipeline to run, gaining peer review. All this takes time and slows developers down.

`main` branch is no longer the Source Of Truth because of this, but that's OK. At any point the CI/CD pipeline can be executed and restore the environment back to "known".  
All deployments must still be executed using terraform - there is still no "ClickOps". This maintains several guardrails:

* The API token used by TFC has a small blast radius (the app environment) and its permissions can be constrained even further
* TFC [Run Tasks](https://developer.hashicorp.com/terraform/cloud-docs/integrations/run-tasks) can be used to enforce guardrails and SAST.
* Sentinel of OPA [policy enforcement](https://developer.hashicorp.com/terraform/cloud-docs/policy-enforcement) is _always_ enforced and there is no way to escape this.

## RBAC

Application teams typically fall into a few standard groups with defined permission needs.

### Developers

Developers typically want write access to development environments so they can rapidly iterate on solving problems. This model enables that
by creating a "developers" identity group and giving it the ability to (a) execute `terraform apply` from the command line and (b) read access to all environments.