# This file configures the AWS credentials needed for the vault
# secrets mount.

# This is all that is allowed in a sandbox account
data "aws_iam_policy" "demo_user_permissions_boundary" {
  name = "DemoUser"
}


resource "aws_iam_user" "vault_mount_user" {
  name                 = "platform-vault-user"
  permissions_boundary = data.aws_iam_policy.demo_user_permissions_boundary.arn
  force_destroy        = true
}


resource "aws_iam_user_policy" "vault_mount_user" {
  user   = aws_iam_user.vault_mount_user.name
  policy = data.aws_iam_policy.demo_user_permissions_boundary.policy
  name   = "PlatformVaultUserPolicy"
}


resource "aws_iam_access_key" "vault_mount_user" {
  user = aws_iam_user.vault_mount_user.name
}
