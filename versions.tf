terraform {

  required_version = "~> 1.4.0" # Matches the version set in the TFC workspace - must be changed in sync

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.67.0"
    }
    doormat = {
      source  = "doormat.hashicorp.services/hashicorp-security/doormat"
      version = "~> 0.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 16.0.0"
    }
    tfe = {
      source  = "hashicorp/tfe"
      version = "~> 0.44.0"
    }
    vault = {
      source  = "hashicorp/vault"
      version = "~> 3.15.0"
    }
  }
}