# Sandbox environments can only use doormat cred
# for anything more substantial than 'describe regions'.
# Normally, vault dynamic provider creds would be used
# (see this working elsewhere in this PoC).
# For my PoC though, I need to use doormat.



# IAM policy that permits the Doormat user to assume our IAM role
data "aws_iam_policy_document" "doormat_assume_role" {
  statement {
    actions = [
      "sts:AssumeRole",
      "sts:SetSourceIdentity",
      "sts:TagSession"
    ]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::397512762488:user/doormatServiceUser"] # infrasec_prod   
    }
  }
}


# Permissions this TFE Workspace will have in my AWS sandbox account
data "aws_iam_policy_document" "doormat" {
  statement {
    actions   = ["*"]
    resources = ["*"]
  }
}

# Role that the PoC TFE workspae will use to create AWS resources
resource "aws_iam_role" "doormat" {
  name               = local.name
  assume_role_policy = data.aws_iam_policy_document.doormat_assume_role.json

  tags = {
    hc-service-uri = "app.terraform.io/${data.environment_variable.tfc_organization.value}/${local.name}"
  }

  inline_policy {
    name   = "doormat-role-permissions"
    policy = data.aws_iam_policy_document.doormat.json
  }
}