# Gets details of the gitlab VCS connection so the workspace
# can be connected to a VCS repo.
data "tfe_oauth_client" "client" {
  service_provider = "gitlab_hosted"
  organization     = data.environment_variable.tfc_organization.value
}

# Each environment has its own TFE workspace
# to keep the blast radius low.
resource "tfe_workspace" "default" {
  name              = local.name
  description       = var.description
  project_id        = var.tfe_project_id
  terraform_version = var.terraform_version
  source_name       = "Platform Bootstrapper"
  source_url        = data.gitlab_project.default.web_url

  tag_names = local.tags

  # If developers have write access then the workspace should use
  # the CLI workflow.
  # If there is no write access (i.e. this is a 'higher' environment like
  # production where git should be the source of truth) then
  # link the workspace to VCS.
  dynamic "vcs_repo" {
    for_each = var.developer_write_access ? [] : [true]
    content {
      identifier     = data.gitlab_project.default.path_with_namespace
      branch         = "main"
      oauth_token_id = data.tfe_oauth_client.client.oauth_token_id
    }
  }
}

# Set the vault role that this workspace should authN to.
resource "tfe_variable" "vault_authn_role" {
  key          = "TFC_VAULT_RUN_ROLE"
  value        = vault_jwt_auth_backend_role.tfc.role_name
  description  = "Name of the JWT role to authN with in vault"
  category     = "env"
  workspace_id = tfe_workspace.default.id
}

# Set the vault role that this workspace should authN to.
resource "tfe_variable" "vault_aws_authn_role" {
  key          = "TFC_VAULT_BACKED_AWS_RUN_VAULT_ROLE"
  value        = vault_aws_secret_backend_role.default.name
  description  = "Name of the AWS role to authN with in vault"
  category     = "env"
  workspace_id = tfe_workspace.default.id
}

# Set the AWS role ARN onto the workspace so it can use the doormat provider to authenticate
resource "tfe_variable" "doormat_arn" {
  key          = "doormat_arn"
  value        = aws_iam_role.doormat.arn
  category     = "terraform"
  workspace_id = tfe_workspace.default.id
  description  = "ARN of the role that doormat uses to authenticate with AWS"
}


# Grant the app developers correct permission to this workspace.
# With write access they can run `terraform apply`, otherwise they
# can `terraform plan`.
resource "tfe_team_access" "developers" {
  team_id      = var.tfc_developers_team_id
  workspace_id = tfe_workspace.default.id

  permissions {
    runs              = var.developer_write_access ? "apply" : "plan"
    variables         = var.developer_write_access ? "write" : "read"
    state_versions    = var.developer_write_access ? "write" : "read-outputs"
    sentinel_mocks    = var.developer_write_access ? "read" : "none"
    workspace_locking = var.developer_write_access
    run_tasks         = false # Controlled by bootstrapper not humans
  }
}
