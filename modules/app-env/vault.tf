
# Role that the bootstrapper TFC workspace can use to authN with vault
resource "vault_jwt_auth_backend_role" "tfc" {
  backend        = "jwt"
  role_name      = local.name
  token_policies = var.vault_policies

  role_type       = "jwt"
  bound_audiences = [data.environment_variable.vault_aud_claim.value]
  bound_claims = {
    terraform_organization_name = tfe_workspace.default.organization
    terraform_workspace_id      = tfe_workspace.default.id
  }
  user_claim = "terraform_full_workspace"
}

# AWS role that can be used by the app / env workspace to provision 
# resources in AWS.
resource "vault_aws_secret_backend_role" "default" {
  backend         = "aws" # Matches vault_aws_secret_backend.aws.path
  credential_type = "assumed_role"
  name            = local.name
  role_arns       = [data.aws_iam_role.vault_target_iam_role.arn]
}


data "aws_iam_role" "vault_target_iam_role" {
  name = "vault-assumed-role-credentials-demo"
}