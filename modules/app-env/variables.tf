variable "app_name_slug" {
  type        = string
  description = "The name of this application in a machine-friendly format (all lower-case, no punctuation other than dash)"

  validation {
    condition     = can(regex("^[a-z][-a-z]+$", var.app_name_slug))
    error_message = "Only hypens and the lower case letters a-z are allowed."
  }
}

variable "environment" {
  type        = string
  description = "The name of this environment. Can be either `dev`, `uat` or `prd`."

  validation {
    condition     = can(regex("^[a-z][a-z0-9]+$", var.environment))
    error_message = "Only lower case letters and numbers are allowed."
  }
}

variable "description" {
  type        = string
  description = "Description to use for this application / environment."
}

variable "tfe_project_id" {
  type        = string
  description = "ID of the project in TFC where this workspace should live."
}

variable "tfc_developers_team_id" {
  type        = string
  description = "ID of the developers team in TFC"
}

variable "terraform_version" {
  type        = string
  description = "Terraform version constraint that is used in this workspace."
}

variable "gitlab_project_id" {
  type        = string
  description = "The ID of the Gitlab Project"
}

variable "developer_write_access" {
  type        = bool
  description = "True if developers should have write access to this environment. Default: `false`. Write access will result in a CLI-driven workflow. Otherwise the workspace will be linked to the VCS workflow and no manual changes can be made."
  default     = false
}

variable "vault_policies" {
  type        = list(string)
  description = "List of vault policy documents that define what secrets this TFC workspace will have access to."
}