locals {

  # Unique name for this app / env.
  name = "${var.app_name_slug}-${var.environment}"

  # The tags on the TFC workspace determine how a developer's CLI
  # commands execute.
  # The `cloud` block in an IaC repo's `backend.tf` file use tags to
  # select the workspace. Developers can use the `terraform workspace`
  # to segregate themselves from other dev environments for their app.
  tags = [
    var.app_name_slug,
    var.environment,
    (var.developer_write_access ? "cli-exec" : "vcs-exec"),
  ]
}

data "environment_variable" "tfc_organization" {
  name = "TFE_ORGANIZATION"
}

data "environment_variable" "vault_aud_claim" {
  name = "TFC_VAULT_WORKLOAD_IDENTITY_AUDIENCE"
}

data "gitlab_project" "default" {
  id = var.gitlab_project_id
}

