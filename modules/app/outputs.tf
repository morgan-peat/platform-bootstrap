output "gitlab_project_id" {
  description = "The ID of the Gitlab Project"
  value       = gitlab_project.project.id
}

output "tfc_project_id" {
  description = "The ID of the TFC project where workspaces for this applications will live."
  value       = tfe_project.default.id
}

output "name_slug" {
  description = "The name of this application in a machine-friendly format (all lower-case, no punctuation other than dash)"
  value       = var.name_slug
}

output "tfc_developers_team_id" {
  description = "ID of the `developers` team created for this app in TFC."
  value       = tfe_team.developers.id
}