variable "name" {
  type        = string
  description = "The friendly name of this application."
}

variable "name_slug" {
  type        = string
  description = "The name of this application in a machine-friendly format (all lower-case, no punctuation other than dash)"

  validation {
    condition     = can(regex("^[a-z][-a-z]+$", var.name_slug))
    error_message = "For the `name_slug` value only hypens and the lower case letters a-z are allowed."
  }
}

variable "description" {
  type        = string
  description = "Description to use for this application"
}

variable "gitlab_namespace_id" {
  type        = string
  description = "The GitLab namespace where this project should be placed."
}

variable "developers" {
  type        = set(string)
  description = "List of usernames to grant `developer` permission to."
}