# Environment variables that are set in the workspace that need to be passed
# on to 'child' workspaces.
data "environment_variable" "vault_address" {
  name = "VAULT_ADDR"
}

data "environment_variable" "vault_namespace" {
  name = "VAULT_NAMESPACE"
}

data "environment_variable" "tfc_organization" {
  name = "TFE_ORGANIZATION"
}

# IaC will live in this repository
# In an enterprise setting, this would probably live inside a specific
# GitLab Group with authZ defined at the group level.
resource "gitlab_project" "project" {
  name         = var.name
  path         = var.name_slug
  description  = var.description
  namespace_id = var.gitlab_namespace_id

  visibility_level       = "public"
  default_branch         = "main"
  initialize_with_readme = true # TODO initialise with template.
}

# Each app lives in its own TFC Project
# (No ability to nest yet, the project structure is flat)
resource "tfe_project" "default" {
  name = var.name_slug
}


# Create a unique TFC team and API token for the TFC project.
# This will be used for automating through the CI/CD pipeline.
# It should _never_ have any human members. It should only ever be
# used to gain an API token.
resource "tfe_team" "api" {
  name = "${var.name_slug}-api"
}

resource "tfe_team_project_access" "api" {
  access     = "write"
  team_id    = tfe_team.api.id
  project_id = tfe_project.default.id
}

resource "tfe_team_token" "api" {
  team_id = tfe_team.api.id
}


# Identity groups would ideally be configured in some
# form of IdP like Azure AD then replicated to all shared
# services.
# For this PoC I manually configure the identity groups.
#
# Note that here we set up _identity_ but not _authorisation_.
# This is because an identity group can be reused between 
# environments. For example, the same group of app developers 
# may have write access to dev but only read access to prd.
resource "tfe_team" "developers" {
  name = "${var.name_slug}-developers"
}

data "tfe_organization_membership" "developers" {
  for_each = var.developers
  username = each.key
}

resource "tfe_team_organization_members" "developers" {
  team_id                     = tfe_team.developers.id
  organization_membership_ids = [for developer in var.developers : data.tfe_organization_membership.developers[developer].id]
}

resource "tfe_team_project_access" "developers" {
  access     = "read"
  team_id    = tfe_team.developers.id
  project_id = tfe_project.default.id
}