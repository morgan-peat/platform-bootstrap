terraform {

  required_version = "~> 1.4.0" # Matches the version set in the TFC workspace - must be changed in sync

  required_providers {
    environment = {
      source  = "MorganPeat/environment"
      version = "~> 0.1"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 16.0.0"
    }
    tfe = {
      source  = "hashicorp/tfe"
      version = "~> 0.44.0"
    }
  }
}