# A set of variables to be applied to all this applications's
# environments (i.e. project-wide).
resource "tfe_variable_set" "vars" {
  name        = "${var.name} variables"
  description = "Variables for all environments."
}

resource "tfe_project_variable_set" "vars" {
  project_id      = tfe_project.default.id
  variable_set_id = tfe_variable_set.vars.id
}


#
# Variables that enable app-env workspaces to authenticate with
# vault using dynamic credentials
#

resource "tfe_variable" "vault_auth" {
  key             = "TFC_VAULT_PROVIDER_AUTH"
  value           = "true"
  category        = "env"
  description     = "Instructs TFC to authN with vault"
  variable_set_id = tfe_variable_set.vars.id
}

resource "tfe_variable" "vault_address" {
  key             = "TFC_VAULT_ADDR"
  value           = data.environment_variable.vault_address.value
  category        = "env"
  description     = "URL of Vault to authN to"
  variable_set_id = tfe_variable_set.vars.id
}

resource "tfe_variable" "vault_namespace" {
  key             = "TFC_VAULT_NAMESPACE"
  value           = data.environment_variable.vault_namespace.value
  category        = "env"
  description     = "Namespace where the vault role lives"
  variable_set_id = tfe_variable_set.vars.id
}

resource "tfe_variable" "vault_jwt_backend" {
  key             = "TFC_VAULT_AUTH_PATH"
  value           = "jwt"
  category        = "env"
  description     = "Mount path of jwt backend"
  variable_set_id = tfe_variable_set.vars.id
}

resource "tfe_variable" "vault_aud_claim" {
  key             = "TFC_VAULT_WORKLOAD_IDENTITY_AUDIENCE"
  value           = "vault.workload.identity"
  category        = "env"
  description     = "Value for aud claim in JWT token"
  variable_set_id = tfe_variable_set.vars.id
}


#
# Variables that enable app-env workspaces to authenticate with
# aws using dynamic credentials from vault.
#

resource "tfe_variable" "vault_aws_auth" {
  key             = "TFC_VAULT_BACKED_AWS_AUTH"
  value           = "true"
  category        = "env"
  description     = "Causes vault to generate AWS creds"
  variable_set_id = tfe_variable_set.vars.id
}

resource "tfe_variable" "vault_aws_auth_type" {
  key             = "TFC_VAULT_BACKED_AWS_AUTH_TYPE"
  value           = "assumed_role"
  category        = "env"
  description     = "The type of aws authentication vault will use."
  variable_set_id = tfe_variable_set.vars.id
}

resource "tfe_variable" "vault_aws_mount_path" {
  key             = "TFC_VAULT_BACKED_AWS_MOUNT_PATH"
  value           = "aws"
  category        = "env"
  description     = "Mount path of aws backend"
  variable_set_id = tfe_variable_set.vars.id
}

data "aws_iam_role" "vault_target_iam_role" {
  name = "vault-assumed-role-credentials-demo"
}

resource "tfe_variable" "vault_aws_role_arn" {
  key             = "TFC_VAULT_BACKED_AWS_RUN_ROLE_ARN"
  value           = data.aws_iam_role.vault_target_iam_role.arn
  category        = "env"
  description     = "ARN of the role to assume in AWS"
  variable_set_id = tfe_variable_set.vars.id
}

resource "tfe_variable" "aws_region" {
  key             = "AWS_REGION"
  value           = "eu-west-2" # London
  category        = "env"
  description     = "AWS region to use"
  variable_set_id = tfe_variable_set.vars.id
}
