# A dummy application to host module repos and CI

module "modules_app" {
  source = "./modules/app"

  name                = "Modules"
  name_slug           = "modules"
  description         = "Dummy application that hosts module repos and CI."
  gitlab_namespace_id = data.gitlab_current_user.default.namespace_id
  developers = [
    "morgan_peat_test",
    "morgan_peat_hashi",
  ]
}

module "modules_app_ci" {
  source = "./modules/app-env"

  app_name_slug = module.modules_app.name_slug
  environment   = "dev"
  description   = "DEV workspace for Modules"

  developer_write_access = true

  tfe_project_id         = module.modules_app.tfc_project_id
  tfc_developers_team_id = module.modules_app.tfc_developers_team_id
  terraform_version      = "~> 1.4.0"
  gitlab_project_id      = module.modules_app.gitlab_project_id
  vault_policies         = ["default", vault_policy.platform_app_env_template.name]
}
