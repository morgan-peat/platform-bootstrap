variable "aws_role_arn" {
  description = "ARN of the role that the AWS terraform provider uses"
  type        = string
}
