# Super-admin policy for the platform bootstrapper
data "vault_policy_document" "platform_app_env_template" {
  rule {
    path         = "*"
    capabilities = ["create", "read", "update", "delete", "list", "sudo"]
    description  = "Default policy for an application environment"
  }
}

resource "vault_policy" "platform_app_env_template" {
  name   = "platform-app-env-template"
  policy = data.vault_policy_document.platform_app_env_template.hcl
}

# Configures the AWS secrets engine
# This allows app environments to authenticate to the aws provider
# using vault dynamic secrets
resource "vault_aws_secret_backend" "aws" {
  access_key  = aws_iam_access_key.vault_mount_user.id
  secret_key  = aws_iam_access_key.vault_mount_user.secret
  description = "AWS dynamic secrets"
  region      = "eu-west-2" # London
  path        = "aws"
}
